from marshmallow import ValidationError


class CustomException(Exception):
    status = None
    message = None

    def __init__(self, message: str = None, detail: str = None):
        self.message = message or self.message
        self.detail = detail
        super(Exception, self).__init__(self.message)

    def __str__(self):
        return f'Exception, message: {self.message}, detail: {self.detail}'


class InternalError(CustomException):
    status = 500


class AuthorizationError(CustomException):
    status = 401
    message = "Неверный логин или пароль!"


class BadRequest(CustomException):
    status = 400


CUSTOM_EXCEPTIONS = {
    ValidationError: BadRequest,
}
