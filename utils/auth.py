import os
from datetime import datetime, timedelta

import jwt
from sanic.request import Request

from users.app.config import config
from utils.exceptions import AuthorizationError


def get_public_key():
    path = os.path.abspath(os.path.join("private_key.pem"))
    with open(path) as file:
        return file.read()


def generate_jwt(payload: dict, headers: dict = None):
    return jwt.encode(
        payload={**payload, 'exp': datetime.utcnow() + timedelta(seconds=config.EXPIRE_JWT_TOKEN)},
        headers=headers or {"alg": "HS256", "typ": "JWT"},
        key=get_public_key(),
        algorithm='HS256',
    )


def auth_jwt(f):
    async def wrapper(*args, **kwargs):
        request: Request = args[0]
        jwt_token = request.headers.get('Authorization')
        if not jwt_token:
            raise AuthorizationError('Пожалуйста, авторизуйтесь!')
        try:
            request.headers['client_info'] = jwt.decode(jwt_token, get_public_key())
            return await f(*args, **kwargs)
        except jwt.InvalidSignatureError:
            AuthorizationError()
    return wrapper
