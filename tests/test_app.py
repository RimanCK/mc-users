from unittest import mock
from unittest.mock import patch, Mock, AsyncMock

from pytest_sanic.utils import TestClient
from tests import data
from tests.db_fixtures import TestWithDb


class TestApp(TestWithDb):
    async def test_registration(self, client: TestClient):
        response = await client.post(
            '/user/registry',
            json=data.registration_user_verona,
        )

        assert response.status == 201

        response = await client.post(
            '/user/registry',
            json=data.registration_user_simona,
        )

        assert response.status == 201

    async def test_registration_bad_validate(self, client: TestClient):
        response = await client.post(
            '/user/registry',
            json={
                "password": "123",
                "email": "123@live.ru"
            }
        )
        json = await response.json()
        assert response.status == 400
        assert json == {
            'description': "Exception, message: {'username': ['Missing data for required field.']}, detail: None",
            'status': 400,
        }

    async def test_unique_registration_user(self, client: TestClient):
        response = await client.post(
            '/user/registry',
            json=data.registration_user_simona,
        )
        json = await response.json()
        assert json == {
            'description': 'Exception, message: Пользователь с таким именем существует!, detail: None',
            'status': 401
        }

    async def test_authorization(self, client: TestClient):
        response = await client.post(
            '/user/auth',
            json=data.auth_user_simona,
        )
        assert response.headers.get('Authorization') != None
        assert response.status == 201

    async def test_authorization_bad(self, client: TestClient):
        response = await client.post(
            '/user/auth',
            json={**data.auth_user_simona, 'username': 'Mark'},
        )
        assert response.headers.get('Authorization') == None
        assert response.status == 401

    async def test_get_user_info(self, client: TestClient):
        response = await client.post(
            '/user/auth',
            json=data.auth_user_simona,
        )
        token = response.headers.get('Authorization')
        assert response.status == 201

        response = await client.get(
            '/user/1',
            headers={'Authorization': token}
        )
        json = await response.json()
        assert json == {
            'email': '123@live.ru',
            'offers': [{}],
            'username': 'Verona',
        }
