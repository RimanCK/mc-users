import pytest
from alembic.command import downgrade, upgrade
from alembic.config import Config as AlembicConfig

from users.api.models import Base
from users.app.config import config
from users.app.db import DBPool


ALEMBIC_CONFIG = 'alembic.ini'


class TestWithDb:

    @pytest.fixture(autouse=True, scope='session')
    def dp_pool(self):
        config = AlembicConfig(ALEMBIC_CONFIG)
        config.attributes['configure_logger'] = False

        upgrade(config, 'head')

        yield 'on head'

        downgrade(config, 'base')

    @pytest.fixture(autouse=True)
    async def clear_data(self, dp_pool, app):
        yield 'I will clear tables for you'

        reference_value_tables = [
            f'{config.DB_SCHEMA}.{name}' for name in (
                'TUSER',
                'TOFFER',
            )
        ]
        db_pool = DBPool()
        await db_pool.init_db(app, loop=None)
        db_pool = await db_pool.get_pool()
        async with db_pool.transaction() as conn:
            for name, table in Base.metadata.tables.items():
                if name not in reference_value_tables:
                    await conn.execute(table.delete())
