import functools

from logging import getLogger
from asyncpg.pool import PoolConnectionProxy
from asyncpgsa.pool import create_pool

from users.app.config import config


logger = getLogger('db')


class DBPool:
    db_pool = None

    @classmethod
    async def init_db(cls, app, loop):
        pool = await create_pool(dsn=config.DB_URL, min_size=3)
        cls.db_pool = pool

    @classmethod
    async def close_db(cls, app):
        await cls.db_pool.close()

    @classmethod
    async def get_pool(cls):
        return cls.db_pool


db_pool_shared = DBPool()


def db_session(f):
    @functools.wraps(f)
    async def wrapper(*args, **kwargs):
        if kwargs.get('db_session') is None and all(not isinstance(arg, PoolConnectionProxy) for arg in args):
            pool = await db_pool_shared.get_pool()
            async with pool.transaction() as transaction:
                return await f(*args, db_session=transaction, **kwargs)
        else:
            return await f(*args, **kwargs)

    return wrapper
