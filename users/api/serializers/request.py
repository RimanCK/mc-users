from marshmallow import (
    Schema,
    fields,
    EXCLUDE,
)


class RegistrationSchema(Schema):
    username = fields.String(required=True)
    password = fields.String(required=True)
    email = fields.Email(required=True)


class AuthorizationSchema(Schema):
    username = fields.String(required=True)
    password = fields.String(required=True)


registration_request = RegistrationSchema(unknown=EXCLUDE)
authorization_request = AuthorizationSchema(unknown=EXCLUDE)
