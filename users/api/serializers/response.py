from marshmallow import (
    Schema,
    fields,
    EXCLUDE,
    pre_dump,
)


class OfferSchema(Schema):
    title = fields.String(required=True)
    text = fields.String(required=True)

    @pre_dump
    def remove_skip_values(self, data, **kwargs):
        return {key: value for key, value in data.items() if value}


class UserInfoSchema(Schema):
    username = fields.String(required=True)
    email = fields.String(required=True)
    offers = fields.Nested(OfferSchema, many=True, required=True)


user_info_response = UserInfoSchema(unknown=EXCLUDE)
