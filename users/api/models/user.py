import sqlalchemy as sa
from sqlalchemy.orm import relationship

from users.api.models.base import BaseMixin, Base


class User(BaseMixin, Base):
    __tablename__ = 'TUSER'
    username = sa.Column('DFUSERNAME', sa.String)
    password = sa.Column('DFPASSWORD', sa.LargeBinary)
    email = sa.Column('DFEMAIL', sa.String)
    offers = relationship('Offer')


class Offer(BaseMixin, Base):
    __tablename__ = 'TOFFER'
    title = sa.Column('DFTITLE', sa.String)
    text = sa.Column('DFTEXT', sa.String)
    user_id = sa.Column('DFUSER_ID', sa.Integer, sa.ForeignKey(User.id))
