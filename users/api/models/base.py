import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base

from users.app.config import config


Base = declarative_base()


class BaseMixin:
    __table_args__ = {'schema': config.DB_SCHEMA}
    id = sa.Column(
        'DFOBJ',
        sa.BigInteger,
        primary_key=True,
        autoincrement=True,
        doc='id записи в БД'
    )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
