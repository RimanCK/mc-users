from sanic import Blueprint, response
from sanic.request import Request

from users.api.serializers import (
    registration_request,
    authorization_request,
)
from users.api.serializers import (
    user_info_response,
)
from users.api.v1 import controllers
from utils import auth

v1_blueprint = Blueprint('my_blueprint')


@v1_blueprint.route('user/registry', methods=['POST'])
async def registration_user(request: Request):
    data = registration_request.load(request.json)
    await controllers.registration_user(**data)
    return response.HTTPResponse(status=201)


@v1_blueprint.route('user/auth', methods=['POST'])
async def authorization_user(request: Request):
    data = authorization_request.load(request.json)
    jwt_token = await controllers.authorization_user(**data)
    return response.HTTPResponse(
        headers={'Authorization': jwt_token.decode()},
        status=201,
    )


@v1_blueprint.route('user/<user_id:int>', methods=['GET'])
@auth.auth_jwt
async def get_user_info(request: Request, user_id: int):
    user_info = await controllers.get_user_info(user_id)
    return response.json(user_info_response.dump(user_info))
