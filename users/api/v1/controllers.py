from logging import getLogger

from sqlalchemy import select, insert, join
from users.api.models import Offer, User
from users.app.config import config
from users.app.db import db_session
from utils import (
    get_hash_password,
    verify_password,
    select_as_model_fieldnames,
)
from utils.auth import generate_jwt
from utils.exceptions import AuthorizationError

logger = getLogger(config.APP_NAME)


@db_session
async def registration_user(
        username: str,
        email: str,
        password: str,
        db_session,
):
    password = get_hash_password(password)
    is_exist_user = await db_session.fetch(
        select([User]).select_from(User).where(User.username == username)
    )

    if is_exist_user:
        raise AuthorizationError('Пользователь с таким именем существует!')

    await db_session.execute(
        insert(User).values(
            {
                User.username: username,
                User.email: email,
                User.password: password,
            },
        ),
    )
    logger.info(f'registered user: {username}')


@db_session
async def authorization_user(
        username: str,
        password: str,
        db_session,
):
    user = await db_session.fetch(
        select([User.username, User.email, User.password]).select_from(User).where(User.username == username),
    )
    if user:
        token = user[0][User.password.expression.description]
        if not verify_password(password, token):
            raise AuthorizationError()
        return generate_jwt(
            payload={
                'username': user[0][User.username.expression.description],
                'email': user[0][User.email.expression.description],
            },
        )
    else:
        raise AuthorizationError()


@db_session
async def get_user_info(
        user_id: int,
        db_session,
):
    users_info = await db_session.fetch(
        select([*select_as_model_fieldnames(User), *select_as_model_fieldnames(Offer)]).select_from(
            join(User, Offer, Offer.user_id == User.id, isouter=True)
        ).where(User.id == user_id),
    )
    if users_info:
        return {
            'username': users_info[0]['username'],
            'email': users_info[0]['email'],
            'offers': [dict(item) for item in users_info]
        } or {}
